# argocd-config


1. Create a local kind cluster
   
   * kind create cluster --name argocd-cluster
   

2. This will install argocd on the cluster on the argocd namespace (including creating it)

   * kustomize build . | kubectl apply -f -
   * kubectl config use-context kind-argocd-cluster --namespace=argocd


3. Application and AppProject CRDs

   * kubectl apply -f apps/argocd-proj.yaml
   * kubectl apply -f apps/argocd-app.yaml


4. Lets make a change to the repo to see how argocd updates itself

   * Upgrade version from v1.7.0-rc1 to v1.7.1


5. Lets add a new cluster to the list, using the cli

   * first create the cluster, I am using AWS EKS and start it with eksctl (https://eksctl.io):
     * eksctl create cluster -n dev-cluster -r eu-west-3
   * kubectl config use-context kind-argocd-cluster --namespace=argocd
   * argocd login localhost:8081 --insecure --name workshop-argocd --username admin --password PASSWORD
   * argocd cluster add CONTEXT

6. What argocd creates in the destination cluster
   
   * ServiceAccount
     * kubectl get sa -n kube-system
     * kubectl get sa argocd-manager -n kube-system -o yaml
   * ClusterRole
     * kubectl get clusterrole
     * kubectl get clusterrole argocd-manager-role -o yaml
   * ClusterRoleBinding
     * kubectl get clusterrolebinding
     * kubectl get clusterrolebinding argocd-manager-role-binding -o yaml


7. What argocd creates in its own cluster

   * a Secret:
     * kubectl config use-context kind-argocd-cluster --namespace argocd
     * kubectl get secrets
     * kubectl get secrets NAME -o yaml
